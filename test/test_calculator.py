"""
Unit tests for the calculator library
"""
from calculator import utils


class TestCalculator:

    def test_addition(self):
        assert 4 == utils.add(2, 2)

    def test_subtraction(self):
        assert 2 == utils.subtract(4, 2)

    def test_multiply(self):
        assert 35 == utils.multiply(7, 5)

    def test_divide(self):
        assert 5 == utils.divide(10, 2)

